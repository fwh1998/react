import React, { useState, useEffect } from 'react';
import Children from './children'
const Test=()=>{
    const [age,setAge] = useState(0)
    const clickMe = ()=>{
       setAge(age+1)
    }

    useEffect(()=>{
        document.title = `${age}`
    })
    return(
        <div>
            <p>{age}</p>
            <button onClick={clickMe}>按钮</button>
            <Children age={age}/>
        </div>
    )
}

export default Test