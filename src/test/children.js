import React from 'react'

const Children = (props)=>{
    const {age} = props
    return(
       <>
       父组件传来的参数{age}
       </>
    )
}

export default Children