import React, { useState, useEffect } from 'react';
import Children from './children'
const Test2=()=>{
    const [age,setAge] = useState(18)
   

    useEffect(()=>{
        document.title = `${age}`
    })
    return(
        <div>
            <p>我是父组件的值{age}</p>
         
            <Children age={age} setAge={setAge}/>
        </div>
    )
}

export default Test2