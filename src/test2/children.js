import React from 'react'

const Children2 = ({age,setAge})=>{
    console.log(setAge)
    const clickMe =()=>{
        setAge(age+1)
    }
    return(
       <>
       子组件的值{age}
       <button onClick={clickMe}>ClickMe</button>     
       </>
    )
}

export default Children2